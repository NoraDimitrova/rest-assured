package TrelloTests;

import io.restassured.response.Response;
import org.testng.annotations.Test;

import static com.telerikacademy.api.tests.Constants.*;
import static com.telerikacademy.api.tests.Constants.TOKEN;
import static com.telerikacademy.api.tests.Endpoints.CARD_ENDPOINT;
import static com.telerikacademy.api.tests.RequestJSON.CREATE_CARD;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;

public class TestAuth {


    @Test()
    public void testAuth() {


        baseURI = "https://trello.com//1/members/me?key=f9c5181c52a7507010ca224d9a7ee63c&token=31d3cf07343fb9a1a94341426b6721b284dd24c1d57700b6b53d5cec4a141861";
        System.out.println(baseURI);


        Response response = given()
                .queryParam("key", API_KEY)
                .queryParam("token", TOKEN)
                .get();


        response.print();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

    }


}

