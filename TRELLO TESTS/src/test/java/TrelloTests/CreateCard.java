package TrelloTests;

import io.restassured.response.Response;
import org.testng.annotations.Test;

import static com.telerikacademy.api.tests.Constants.*;
import static com.telerikacademy.api.tests.Endpoints.CARD_ENDPOINT;
import static com.telerikacademy.api.tests.RequestJSON.CREATE_CARD;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import static java.lang.String.format;
import static org.testng.Assert.assertEquals;


public class CreateCard {

    @Test()
    public void createCardTest() {

//  {{baseTrello}}/1/cards?idList={{idListMyList}}&key={{APIkey}}&token={{Token}}


        baseURI = format("%s%s%s%s%s%s%s", BASE_URI, CARD_ENDPOINT, IDLISTMYLIST, "&key=", API_KEY, "&token=", TOKEN);
        System.out.println(baseURI);

        String requestBody=(format(CREATE_CARD));
        System.out.println(requestBody);

        Response response = given()
                .queryParam("idList", IDLISTMYLIST)
                .queryParam("key", API_KEY)
                .queryParam("token", TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post();


        response.print();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

    }


}
