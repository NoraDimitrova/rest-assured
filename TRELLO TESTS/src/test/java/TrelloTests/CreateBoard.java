package TrelloTests;

import io.restassured.response.Response;
import org.testng.annotations.Test;

import static com.telerikacademy.api.tests.Constants.*;
import static com.telerikacademy.api.tests.Constants.TOKEN;
import static com.telerikacademy.api.tests.Endpoints.BOARD_ENDPOINT;
import static com.telerikacademy.api.tests.Endpoints.CARD_ENDPOINT;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;

public class CreateBoard {

    @Test()
    public void createBoardTest() {

//  {{baseTrello}}/1/boards/?key={{APIkey}}&token={{Token}}&name=Homework


        baseURI = format("%s%s%s%s%s", BASE_URI, BOARD_ENDPOINT, API_KEY, "&token=", TOKEN);
        System.out.println(baseURI);

        Response response = given()
                .queryParam("name", NAME)
                .queryParam("key", API_KEY)
                .queryParam("token", TOKEN)
                .contentType("application/json")
                .when()
                .post();


        response.print();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");


    }


}
