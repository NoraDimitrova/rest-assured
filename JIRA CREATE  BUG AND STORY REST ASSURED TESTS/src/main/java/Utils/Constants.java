package Utils;

public class Constants {

    /*JIRA API constants*/
    public static final String EMAIL = "nora.dimitrova.a38@learn.telerikacademy.com";
    public static final String TOKEN = "OwAw1U1Uds7WqP6MTZiEA2C3";
    public static final String BASE_JIRA = "https://noradimitrova.atlassian.net";
    public static String DEFAULT_ACCOUNT_ID = "6273f255a1de41006851328a";


    public static final String KEY_PROJECT = "MPVJAP";

    public static final String STORY_SUMMARY = "The user wants to see information about the Travel Agency.";
    public static final String BUG_SUMMARY = "Travel Agency information is not displayed when the user clicks on the About Us button.";
    public static final String STORY_DESCRIPTION = "Narrative:\\n\\nAs a User,\\n\\n"
            +"I want to see information about the Travel Agency.\\n\\n"
            +"Steps to reproduce:\\n\\n"
            +" 1. Navigate to URL - www.phptravels.net/ru \\n\\n"
            +" 2. Go to the down left corner and click on the About Us button.\\n\\n"
            +"Expected result:\\n\\nI Find information about the Travel Agency.\\n\\n"
            +"Severity: Major";

    public static final String BUG_DESCRIPTION = "Narrative:\\n\\nAs a User, \\n\\n"
            +"I want to find inforamtion about the Travel Agency, \\n\\n"
            +"I can't find information\\n\\nSteps to reproduce:\\n\\n"
            +" 1. Navigate to URL - www.phptravels.net/ru \\n\\n"
            +" 2. Go to the down left corner and click on the About Us button. \\n\\n"
            +" 3. View information on the screen.\\n\\n"
            +"Expected result:\\n\\nInformation about the Тravel Аgency appears on the screen.\\n\\n"
            +"Actual result:\\n\\nInformation how to get the Mobile Application appears on the screen.\\n\\n\\n\\"
            +"nSeverity: Critical";
    public static final String STORY_NAME = "Story";
    public static String STORY_KEY;

    public static final String BUG_NAME = "Bug";
    public static String BUG_KEY;


}
