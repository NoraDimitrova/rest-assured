package Utils;

import groovyjarjarantlr4.v4.analysis.AnalysisPipeline;

import java.security.PublicKey;

import static java.lang.String.format;

public class Endpoints {

    public static final String API_VERSION = "/rest/api/2";

    public static final String ISSUE_ENDPOINT = format("%s%s", API_VERSION, "/issue");

    public static final String LINK_ENDPOINT = format("%s%s", API_VERSION, "/issueLink");

    public static final String TRANSITION_ENDPOINT = format("%s", "/transitions");

    public static final String ISSUE_TRANSITION = format("%s", "/MPVJAP-1/transitions");

    public static final String ATTACHMENT_ENDPOINT=format("%s", "/attacments");

}
