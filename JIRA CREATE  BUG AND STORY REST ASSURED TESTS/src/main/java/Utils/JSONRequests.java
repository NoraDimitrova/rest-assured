package Utils;

public class JSONRequests {

    public static final String CREATE_ISSUE_STORY = "{\n" +
            "    \"fields\": {\n" +
            "       \"project\":\n" +
            "       {\n" +
            "          \"key\": \"%s\"\n" +
            "       },\n" +
            "       \"summary\": \"%s\",\n" +
            "       \"description\": \"%s\",\n" +
            "       \"issuetype\": {\n" +
            "          \"name\": \"%s\"\n" +
            "       }\n" +
            "   }\n" +
            "}";

    public static final String CREATE_ISSUE_BUG = "{\n" +
            "    \"fields\": {\n" +
            "       \"project\":\n" +
            "       {\n" +
            "          \"key\": \"%s\"\n" +
            "       },\n" +
            "       \"summary\": \"%s\",\n" +
            "       \"description\": \"%s\",\n" +
            "       \"issuetype\": {\n" +
            "          \"name\": \"%s\"\n" +
            "       }\n" +
            "   }\n" +
            "}";


    public static final String SET_STORY_PRIORITY_AND_ENVIRONMENT = "{\n" +
            "    \"fields\": {\n" +
            "       \"project\":\n" +
            "       {\n" +
            "          \"key\": \"%s\"\n" +
            "       },       \n" +
            "       \n" +
            "       \"environment\":\"https://www.phptravels.net/ru\",\n" +
            "       \n" +
            "       \"priority\":{\n" +
            "          \"name\":\"High\"\n" +
            "       }\n" +
            "   }\n" +
            "}";

    public static final String SET_BUG_PRIORITY_AND_ENVIRONMENT = "{\n" +
            "    \"fields\": {\n" +
            "       \"project\":\n" +
            "       {\n" +
            "          \"key\": \"%s\"\n" +
            "       },       \n" +
            "       \n" +
            "       \"environment\":\"https://www.phptravels.net/ru\",\n" +
            "       \n" +
            "       \"priority\":{\n" +
            "          \"name\":\"Highest\"\n" +
            "       }\n" +
            "   }\n" +
            "}";

    public static final String LINK_BUG_TO_STORY = "{\n" +
            "    \"type\": {\n" +
            "        \"name\": \"Blocks\",\n" +
            "         \"inward\":\"is blocked by\",\n" +
            "         \"outward\":\"blocks\"\n" +
            "    },\n" +
            "    \"inwardIssue\": {\n" +
            "        \"key\": \"%s\"\n" +
            "    },\n" +
            "    \"outwardIssue\": {\n" +
            "        \"key\": \"%s\"\n" +
            "    }\n" +
            "}\n";


    public static final String STORY_TRANSITION = "{ \n" +
            " \"transition\": {\n" +
            " \"id\": \"21\"\n" +
            " }\n" +
            "}";

    public static final String BUG_TRANSITION = "{\n" +
            " \"update\": {\n" +
            " \"comment\": [\n" +
            " {\n" +
            " \"add\": {\n" +
            " \"body\": \"Bug has been fixed\"\n" +
            " }\n" +
            " }\n" +
            " ]\n" +
            " },\n" +
            " \"transition\": {\n" +
            " \"id\": \"31\"\n" +
            " }\n" +
            "}";

    public static final String SET_ASSIGNEE_FIELD = "{\n" +
            "    \"fields\": {\n" +
            "       \"project\":\n" +
            "       {\n" +
            "          \"key\": \"MPVJAP\"\n" +
            "       },\n" +
            "       \"assignee\":{\n" +
            "          \"id\":\"%s\"\n" +
            "       }       \n" +
            "   }\n" +
            "}";



}
