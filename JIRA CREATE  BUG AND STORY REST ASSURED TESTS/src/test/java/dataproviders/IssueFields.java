package dataproviders;

import org.testng.annotations.DataProvider;

import static Utils.Constants.*;

public class IssueFields {

    @DataProvider
    public static Object[][] fieldsPerIssue() {
        Object[][] dataset = new Object[2][4];

        dataset[0][0] = KEY_PROJECT;
        dataset[0][1] = STORY_SUMMARY;
        dataset[0][2] = STORY_DESCRIPTION;
        dataset[0][3] = STORY_NAME;

        dataset[1][0] = KEY_PROJECT;
        dataset[1][1] = BUG_SUMMARY;
        dataset[1][2] = BUG_DESCRIPTION;
        dataset[1][3] = BUG_NAME;

        return dataset;
    }
}
