package dataproviders;

import org.testng.annotations.DataProvider;

import static Utils.Constants.BUG_KEY;
import static Utils.Constants.STORY_KEY;

public class JIRAPriorities {

    @DataProvider
    public static Object[][] priorityPerIssue() {
        Object[][] dataset = new Object[2][2];

        dataset[0][0] = STORY_KEY;
        dataset[0][1] = "High";

        dataset[1][0] = BUG_KEY;
        dataset[1][1] = "Highest";

        return dataset;
    }
}
