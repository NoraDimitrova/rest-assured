package jira.api.test;

import io.restassured.response.Response;
import org.testng.annotations.Test;

import static Utils.Constants.*;
import static Utils.Endpoints.ISSUE_ENDPOINT;
import static Utils.Helper.isValid;
import static Utils.JSONRequests.SET_BUG_PRIORITY_AND_ENVIRONMENT;
import static Utils.JSONRequests.SET_STORY_PRIORITY_AND_ENVIRONMENT;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SetPriorityAndEnvironment {

    @Test(priority = 3)
    public void createStoryTest() {

        baseURI = format("%s%s/%s", BASE_JIRA, ISSUE_ENDPOINT, STORY_KEY);
        String requestBody = (format(SET_STORY_PRIORITY_AND_ENVIRONMENT, KEY_PROJECT));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .put();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 204, "Incorrect status code. Expected 204.");

        assertEquals(response.body().asString(), "", "Response body is not empty");
    }

    @Test(priority = 3)
    public void createBugTest() {
        baseURI = format("%s%s/%s", BASE_JIRA, ISSUE_ENDPOINT, BUG_KEY);
        String requestBody = (format(SET_BUG_PRIORITY_AND_ENVIRONMENT, KEY_PROJECT));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .put();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 204, "Incorrect status code. Expected 204.");

        assertEquals(response.body().asString(), "", "Response body is not empty");
    }
}
