package jira.api.test;

import io.restassured.response.Response;
import org.testng.annotations.Test;

import static Utils.Constants.*;
import static Utils.Endpoints.ISSUE_ENDPOINT;
import static Utils.Helper.isValid;
import static Utils.JSONRequests.SET_ASSIGNEE_FIELD;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SetAssigneeField {

    @Test(priority=6)
    public void testSetAssigneeFieldToBug(){

        baseURI=format("%s%s/%s", BASE_JIRA, ISSUE_ENDPOINT, BUG_KEY);
        System.out.println(baseURI);

        String requestBody=(format(SET_ASSIGNEE_FIELD, DEFAULT_ACCOUNT_ID));
        System.out.println(requestBody);

        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response=given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .put();

        int statusCode=response.getStatusCode();
        assertEquals(statusCode, 204, "Incorrect status code. Expected 204");
        assertEquals(response.body().asString(), "", "Response body is not Empty");
    }

    @Test(priority=6)
    public void testSetAssigneeFieldToStory(){

        baseURI=format("%s%s/%s", BASE_JIRA, ISSUE_ENDPOINT, STORY_KEY);
        System.out.println(baseURI);

        String requestBody=(format(SET_ASSIGNEE_FIELD, DEFAULT_ACCOUNT_ID));
        System.out.println(requestBody);

        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response=given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .put();

        int statusCode=response.getStatusCode();
        assertEquals(statusCode, 204, "Incorrect status code. Expected 204");
        assertEquals(response.body().asString(), "", "Response body is not Empty");
    }


}
