package jira.api.test;

import io.restassured.response.Response;
import org.testng.annotations.Test;

import static Utils.Constants.*;
import static Utils.Endpoints.LINK_ENDPOINT;
import static Utils.Helper.isValid;
import static Utils.JSONRequests.LINK_BUG_TO_STORY;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LinkBugToStory {

    @Test(priority = 4)
    public void linkBugToStory() {

        baseURI = format("%s%s", BASE_JIRA, LINK_ENDPOINT);

        String requestBody = (format(LINK_BUG_TO_STORY, BUG_KEY, STORY_KEY));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 201, "Incorrect status code. Expected 201.");
        assertEquals(response.body().asString(), "", "Response body is not empty");

    }
}
