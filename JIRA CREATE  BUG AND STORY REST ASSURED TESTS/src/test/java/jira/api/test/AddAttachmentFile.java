package jira.api.test;

import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.io.File;

import static Utils.Constants.*;

import static Utils.Endpoints.ISSUE_ENDPOINT;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;

public class AddAttachmentFile {

    @Test(priority = 7)
    public void addAttachmentToBugTest() {

        baseURI = format("%s%s/%s/attachments", BASE_JIRA, ISSUE_ENDPOINT, BUG_KEY);

        Response response = given()
                .header("X-Atlassian-Token", "no-check")
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("multipart/form-data")
                .multiPart("file", new File("src/test/resources/Picture3.docx"))
                .when()
                .post();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code.");
        assertEquals(response.getBody().jsonPath().get("mimeType[0]"), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "Mime types do not match.");
    }

    @Test(priority = 7)
    public void addAttachmentToStoryTest() {

        baseURI = format("%s%s/%s/attachments", BASE_JIRA, ISSUE_ENDPOINT, STORY_KEY);

        Response response = given()
                .header("X-Atlassian-Token", "no-check")
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("multipart/form-data")
                .multiPart("file", new File("src/test/resources/cat.jpg"))
                .when()
                .post();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code.");
        assertEquals(response.getBody().jsonPath().get("mimeType[0]"), "image/jpeg", "Mime types do not match.");
    }



}
