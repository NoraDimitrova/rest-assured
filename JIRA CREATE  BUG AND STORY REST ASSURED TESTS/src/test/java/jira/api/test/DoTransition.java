package jira.api.test;

import io.restassured.response.Response;
import org.testng.annotations.Test;

import static Utils.Constants.*;
import static Utils.Endpoints.*;
import static Utils.Helper.isValid;
import static Utils.JSONRequests.BUG_TRANSITION;
import static Utils.JSONRequests.STORY_TRANSITION;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class DoTransition {

    @Test(priority = 5)
    public void setStoryTransitionTest() {

        baseURI = format("%s%s/%s%s", BASE_JIRA, ISSUE_ENDPOINT, STORY_KEY, TRANSITION_ENDPOINT);
        System.out.println(baseURI);

        String requestBody = (format(STORY_TRANSITION));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 204, "Incorrect status code. Expected 204.");
        assertEquals(response.body().asString(), "", "Response body is not empty");
    }

    @Test(priority = 5)
    public void setBugTransitionTest() {

        baseURI = format("%s%s/%s%s", BASE_JIRA, ISSUE_ENDPOINT, BUG_KEY, TRANSITION_ENDPOINT);
        System.out.println(baseURI);

        String requestBody = (format(BUG_TRANSITION));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 204, "Incorrect status code. Expected 204.");
        assertEquals(response.body().asString(), "", "Response body is not empty");
    }


    @Test(priority = 5)
    public void getBugTransitionTest() {

        baseURI = format("%s%s/%s%s", BASE_JIRA, ISSUE_ENDPOINT, BUG_KEY, TRANSITION_ENDPOINT);
        System.out.println(baseURI);

        Response response = given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .when()
                .get();

        int statusCode = response.getStatusCode();
        response.getBody().print();


        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        System.out.println(response);
    }



}
