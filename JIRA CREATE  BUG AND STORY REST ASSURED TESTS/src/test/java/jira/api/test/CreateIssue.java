package jira.api.test;

import io.restassured.response.Response;
import org.testng.annotations.Test;
import dataproviders.IssueFields;

import static Utils.Constants.*;
import static Utils.Endpoints.ISSUE_ENDPOINT;
import static Utils.Helper.isValid;
import static Utils.JSONRequests.CREATE_ISSUE_BUG;
import static Utils.JSONRequests.CREATE_ISSUE_STORY;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static io.restassured.RestAssured.baseURI;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class CreateIssue {

    @Test(priority = 1)
    public void createStoryTest() {

        baseURI = format("%s%s", BASE_JIRA, ISSUE_ENDPOINT);
        String requestBody = (format(CREATE_ISSUE_STORY, KEY_PROJECT, STORY_SUMMARY, STORY_DESCRIPTION, STORY_NAME));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 201, "Incorrect status code. Expected 201.");

        STORY_KEY = response.getBody().jsonPath().get("key");
        System.out.printf("Story with key %s was created%n", STORY_KEY);
    }

    @Test(priority = 2)
    public void createBugTest() {

        baseURI = format("%s%s", BASE_JIRA, ISSUE_ENDPOINT);
        String requestBody = (format(CREATE_ISSUE_BUG, KEY_PROJECT, BUG_SUMMARY, BUG_DESCRIPTION, BUG_NAME));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = given()
                .auth().preemptive().basic(EMAIL, TOKEN)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 201, "Incorrect status code. Expected 201.");

        BUG_KEY = response.getBody().jsonPath().get("key");
        System.out.printf("Bug with key %s was created%n", BUG_KEY);
    }
}
